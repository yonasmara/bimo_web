'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';

const RESOURCES = {"assets/AssetManifest.bin": "e1aba2e007f1217699db14297daff577",
"assets/AssetManifest.json": "25dffae0678e304d64abd2f3201682ac",
"assets/assets/fonts/NotoSansJP/NotoSansJP-Bold.otf": "0ff89c4f64cd8f24f68a3034fbfefec1",
"assets/assets/fonts/NotoSansJP/NotoSansJP-Regular.otf": "2bbd3068a53daa60b61f29c3ee9531e7",
"assets/assets/fonts/NotoSansJP/NotoSansJP-Thin.otf": "c7ab53b034c756f7628423e23c62efca",
"assets/assets/images/boarding/1.png": "ecba412404016758ce301cf8d21d845f",
"assets/assets/images/boarding/2.png": "153e2de5c47d2f1b2672938a12235bc8",
"assets/assets/images/boarding/3.png": "b7f0200eaff97235e7f0f41a4805acb0",
"assets/assets/images/denah_filkom.png": "4afb36027896aaa692de513dd02473fd",
"assets/assets/images/denah_map_preview.PNG": "198977facb1142a5282e75361f07c602",
"assets/assets/images/Eskalasi.png": "2e2194c4fd062d9c8b717a6f8b616600",
"assets/assets/images/icon.jpg": "6ca300b514587cb86d24a33ee3f87c0c",
"assets/assets/images/icon_bimo.png": "46859e232bf41e0508cf4eb1a284f3c4",
"assets/assets/images/icon_bimo_blue.png": "7da0b8f6cee71ce18b6c3aac99b0b988",
"assets/assets/images/instruksi_kontak_dosen_pa.png": "9b4af0e4541ace79b7fcb70ea37c4c5a",
"assets/assets/images/instruksi_logbook.png": "2aa1c91bfd38600f59e9b3d10c761189",
"assets/assets/images/IPK-SKS.png": "59cfb2aced10af9d6483d8669b87b2ee",
"assets/assets/images/Logbook.png": "a9d5fca2243e0ffdc3aed3a1fcb416c0",
"assets/assets/images/logo_filkom.png": "b220140aab4cd76e767d0ab8f27a286f",
"assets/assets/images/Milestone.png": "230a0236a5268c0d9b184a726ec8c2e9",
"assets/assets/images/milestone_indikator_kkn.png": "2e797686c4087cd341538fa1093f9345",
"assets/assets/images/milestone_indikator_kuliah.png": "310711face493660c59da3aa7865261a",
"assets/assets/images/milestone_indikator_skripsi.png": "a494d09cf89f3af45bf779a5537590ad",
"assets/assets/images/milestone_indikator_wisuda.png": "824d9d69801207524d3b290723ec290d",
"assets/assets/images/placeholder.png": "85f95e20c51b4e1b9464d43e3445a6a3",
"assets/assets/images/Portofolio.png": "8298a7320ffbcce28f9f8d88d5eea63f",
"assets/assets/images/Rancangan.png": "7c9d139342819f25f7837f38e086b613",
"assets/assets/images/splash.png": "2aff17f56a561d18c7938447ab657c82",
"assets/assets/images/wave.png": "ff5b1cadb6bc1d57e23057484710db39",
"assets/FontManifest.json": "0633e7469d796599f29799eceaeca5cb",
"assets/fonts/MaterialIcons-Regular.otf": "40abbf417cfa1a77360eac9294bc5441",
"assets/NOTICES": "2c0ca4b2bef07c271317a9f065e55339",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "57d849d738900cfd590e9adc7e208250",
"assets/packages/flutter_image_compress_web/assets/pica.min.js": "6208ed6419908c4b04382adc8a3053a2",
"assets/packages/line_icons/lib/assets/fonts/LineIcons.ttf": "bcaf3ba974cf7900b3c158ca593f4971",
"assets/packages/map_launcher/assets/icons/amap.svg": "00409535b144c70322cd4600de82657c",
"assets/packages/map_launcher/assets/icons/apple.svg": "6fe49a5ae50a4c603897f6f54dec16a8",
"assets/packages/map_launcher/assets/icons/baidu.svg": "22335d62432f9d5aac833bcccfa5cfe8",
"assets/packages/map_launcher/assets/icons/citymapper.svg": "58c49ff6df286e325c21a28ebf783ebe",
"assets/packages/map_launcher/assets/icons/doubleGis.svg": "ab8f52395c01fcd87ed3e2ed9660966e",
"assets/packages/map_launcher/assets/icons/google.svg": "cb318c1fc31719ceda4073d8ca38fc1e",
"assets/packages/map_launcher/assets/icons/googleGo.svg": "cb318c1fc31719ceda4073d8ca38fc1e",
"assets/packages/map_launcher/assets/icons/here.svg": "aea2492cde15953de7bb2ab1487fd4c7",
"assets/packages/map_launcher/assets/icons/mapswithme.svg": "87df7956e58cae949e88a0c744ca49e8",
"assets/packages/map_launcher/assets/icons/osmand.svg": "639b2304776a6794ec682a926dbcbc4c",
"assets/packages/map_launcher/assets/icons/osmandplus.svg": "31c36b1f20dc45a88c283e928583736f",
"assets/packages/map_launcher/assets/icons/petal.svg": "76c9cfa1bfefb298416cfef6a13a70c5",
"assets/packages/map_launcher/assets/icons/tencent.svg": "4e1babec6bbab0159bdc204932193a89",
"assets/packages/map_launcher/assets/icons/tomtomgo.svg": "493b0844a3218a19b1c80c92c060bba7",
"assets/packages/map_launcher/assets/icons/waze.svg": "311a17de2a40c8fa1dd9022d4e12982c",
"assets/packages/map_launcher/assets/icons/yandexMaps.svg": "3dfd1d365352408e86c9c57fef238eed",
"assets/packages/map_launcher/assets/icons/yandexNavi.svg": "bad6bf6aebd1e0d711f3c7ed9497e9a3",
"assets/shaders/ink_sparkle.frag": "f8b80e740d33eb157090be4e995febdf",
"canvaskit/canvaskit.js": "76f7d822f42397160c5dfc69cbc9b2de",
"canvaskit/canvaskit.wasm": "f48eaf57cada79163ec6dec7929486ea",
"canvaskit/chromium/canvaskit.js": "8c8392ce4a4364cbb240aa09b5652e05",
"canvaskit/chromium/canvaskit.wasm": "fc18c3010856029414b70cae1afc5cd9",
"canvaskit/skwasm.js": "1df4d741f441fa1a4d10530ced463ef8",
"canvaskit/skwasm.wasm": "6711032e17bf49924b2b001cef0d3ea3",
"canvaskit/skwasm.worker.js": "19659053a277272607529ef87acf9d8a",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"flutter.js": "6b515e434cea20006b3ef1726d2c8894",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"index.html": "ccb476e554a2937bc62c1d62c9b3bea3",
"/": "ccb476e554a2937bc62c1d62c9b3bea3",
"main.dart.js": "5aade6080a307788aa7425f382a3c4bd",
"manifest.json": "34afe301b451fcadf3d15e31cb534015",
"splash/img/dark-1x.png": "494dd9b1be600d5e4ff8127ac4aa93bd",
"splash/img/dark-2x.png": "573f71e7548508eed4006504255a3970",
"splash/img/dark-3x.png": "b8f04526879c59cbb663f99ed0ac0ac1",
"splash/img/dark-4x.png": "a022151efd3044fc94b9903c30a75584",
"splash/img/light-1x.png": "494dd9b1be600d5e4ff8127ac4aa93bd",
"splash/img/light-2x.png": "573f71e7548508eed4006504255a3970",
"splash/img/light-3x.png": "b8f04526879c59cbb663f99ed0ac0ac1",
"splash/img/light-4x.png": "a022151efd3044fc94b9903c30a75584",
"version.json": "38fd57c8af743fb42abc4f9b658d801a"};
// The application shell files that are downloaded before a service worker can
// start.
const CORE = ["main.dart.js",
"index.html",
"assets/AssetManifest.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});
// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        // Claim client to enable caching on first launch
        self.clients.claim();
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      // Claim client to enable caching on first launch
      self.clients.claim();
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});
// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});
self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});
// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
